package com.calamandrei.lorenzo.RandomTimedCommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Command
 *
 * @author Lorenzo Calamandrei
 * @version 0.1.0
 * @since 0.1.0
 */
public class Command {
    private final String name;
    private final String time;
    private final int min, max;
    private int id, cycles, rand;
    private boolean running = false;
    private List<String> commands = new ArrayList<>();

    Command(String name, String time, int min, int max, List<String> commands) {
        this.name = name;
        this.time = time;
        this.min = min;
        this.max = max;
        this.commands = commands;
    }

    public String isRunningInformation() {
        if (running) {
            return ChatColor.BOLD + "* " + ChatColor.GREEN + "" + ChatColor.BOLD + "" + name + " [Running]";
        } else {
            return ChatColor.BOLD + "* " + ChatColor.RED + "" + ChatColor.BOLD + "" + name + " [Stopped]";
        }
    }

    public void terminate() {
        if (!running) {
            return;
        }
        Bukkit.getScheduler().cancelTask(id);
        running = false;
    }

    private int generateRandom() {
        return (int) (min + (Math.random() * (max - min + 1)));
    }

    public void start(JavaPlugin plugin, CommandSender sender) {
        if (running) {
            return;
        }

        rand = generateRandom();
        running = true;
        id = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (cycles >= rand) {
                runCommands();
                rand = generateRandom();
                cycles = 0;
            }
            cycles = cycles + 1;
        }, 20, 20);
        sender.sendMessage(ChatColor.GREEN + "" + name + " job successfully started.");
    }

    public void execute(CommandSender sender) {
        runCommands();
        sender.sendMessage(ChatColor.GREEN + "" + name + " successfully executed.");
    }

    public void timeUntil(CommandSender sender) {
        if (!running) {
            sender.sendMessage(ChatColor.LIGHT_PURPLE + "" + name + " not running.");
            return;
        }
        sender.sendMessage(ChatColor.GREEN + "" + name + "" + time + "" + (rand - cycles) + " seconds until execution.");
    }

    public void stop(CommandSender sender) {
        if (!running) {
            return;
        }

        Bukkit.getScheduler().cancelTask(id);
        running = false;
        sender.sendMessage(ChatColor.GREEN + "" + name + " stopped.");
    }

    public void runCommands() {
        for (String command:commands) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
        }
        Bukkit.getServer().getConsoleSender()
                .sendMessage(ChatColor.GREEN + "Command " + name + " successfully executed.");
    }

    public String getName() {
        return name;
    }
}
