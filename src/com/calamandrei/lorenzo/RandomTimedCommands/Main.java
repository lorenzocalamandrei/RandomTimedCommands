package com.calamandrei.lorenzo.RandomTimedCommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Main
 *
 * @author Lorenzo Calamandrei
 * @version 0.1.0
 * @since 0.1.0
 */
public class Main extends JavaPlugin {
    private static final List<Command> commandList = new ArrayList<>();
    private final List<String> names = new ArrayList<>();

    @Override
    public void onEnable() {
        saveDefaultConfig();
        reloadConfig();
        registerCommands();
    }

    @Override
    public void onDisable() {
        // terminate commands
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        if (!label.equalsIgnoreCase("rct")) {
            return false;
        }

        if (!sender.hasPermission("rct.admin")) {
            sender.sendMessage(ChatColor.RED + "No permission to execute this command!");
            return false;
        }

        // Per job action
        if (names.contains(args[0]) && args.length == 2) {
            String commandName = args[0];
            Command commandToHandle = null;
            for (Command commandSearch:commandList) {
                if (commandSearch.getName().equalsIgnoreCase(commandName)) {
                    commandToHandle = commandSearch;
                }
            }
            if (commandToHandle == null) {
                sender.sendMessage(ChatColor.RED + "Command not exists");
                return false;
            }
            switch (args[1]) {
                case "start":
                    commandToHandle.start(this, sender);
                    break;
                case "execute":
                    commandToHandle.execute(sender);
                    break;
                case "time":
                    commandToHandle.timeUntil(sender);
                    break;
                case "stop":
                default:
                    commandToHandle.stop(sender);
            }
            return true;
        }

        // Standard action
        switch (args[0]) {
            case "reload":
                Bukkit.getPluginManager().disablePlugin(this);
                Bukkit.getPluginManager().enablePlugin(this);
                sender.sendMessage(ChatColor.GREEN + "RCT was successfully reloaded!");
                break;
            case "list":
                listCommands(sender);
                break;
            case "stopAll":
                terminateCommands();
                break;
            case "help":
            default:
                sendHelp(sender);
        }

        return true;
    }

    private void terminateCommands() {
        for (Command command : commandList) {
            command.terminate();
        }
    }

    private void registerCommands() {
        terminateCommands();
        commandList.clear();

        FileConfiguration config = getConfig();
        ConfigurationSection scheduledConfig = config.getConfigurationSection("schedule");
        assert scheduledConfig != null;
        names.addAll(scheduledConfig.getKeys(false));
        for (String key : scheduledConfig.getKeys(false)) {
            String pathPrefix = "schedule." + key + ".";
            String time = config.getString(pathPrefix + "time");
            int min = Integer.parseInt(Objects.requireNonNull(config.getString(pathPrefix + "minTime")));
            int max = Integer.parseInt(Objects.requireNonNull(config.getString(pathPrefix + "maxTime")));
            List<String> commands = config.getStringList(pathPrefix + "commands");
            Command command = new Command(key, time, min, max, commands);
            commandList.add(command);
            if (config.getBoolean(pathPrefix + "runByDefault")) {
                Bukkit.getServer().getConsoleSender().sendMessage("Command " + key + " is auto starting...");
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Bukkit.getServer().dispatchCommand(
                                Bukkit.getServer().getConsoleSender(),
                                "rct " + key + " start"
                        );
                    }
                }.runTaskLater(this, 400);
            }
        }
    }

    private void listCommands(CommandSender sender) {
        sender.sendMessage("~~~~~~~~~~~[RCT Commands]~~~~~~~~~~~~");
        for (Command command : commandList) {
            sender.sendMessage(command.isRunningInformation());
        }
        sender.sendMessage("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage("~~~~~~~~~~~~~~~[RCT Help]~~~~~~~~~~~~~~~~");
        sender.sendMessage("/rct reload");
        sender.sendMessage("- reload the plugin");
        sender.sendMessage("/rct list");
        sender.sendMessage("- lists all registered timed commands");
        sender.sendMessage("/rct stopAll");
        sender.sendMessage("- stops all running timed commands");
        sender.sendMessage("/rct <commandName> [start/stop]/execute/time");
        sender.sendMessage("- start or stop a command");
        sender.sendMessage("- execute a command immediately");
        sender.sendMessage("- display time left until next execution");
        sender.sendMessage("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
